package br.com.digio.api.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.amazonaws.HttpMethod;

import br.com.digio.api.exceptionhandler.Problem;
import br.com.digio.api.exceptionhandler.ProblemType;
import br.com.digio.api.model.ContaContabil;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.model.StatusLancamento;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.response.LancamentoContaContabilCadastroResponse;
import br.com.digio.api.presentation.dto.response.LancamentoContaContabilResponse;
import br.com.digio.infraestructure.repository.ContaContabilRepository;
import br.com.digio.infraestructure.repository.LancamentoContabilRepository;
import br.com.digio.utils.ConstantesResourceAPI;
import br.com.digio.utils.Util;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

/**
 * 
 * @author davidalmeida
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test.properties")
@EnableJpaRepositories(basePackages = "br.com.digio.infraestructure.repository")
public class MovimentacaoContaContabilResourceTest  {
	
	@LocalServerPort
	private int port;
	
	public static String BASEPATH = "/lancamentos-contabeis";
	
	private ContaContabil contaContabilExistente;
	
	private static final Long NUMEROCONTACONTABILCRIAR = 44921L;
	
	@Autowired
	private ContaContabilRepository contaContabilRepository;
	
	@Autowired
	private LancamentoContabilRepository lancamentoRepository;
	
	ResultadoEstatistica resultadoEstatistica;
	
	List<LancamentoContabil> lancamentos = Lists.newArrayList();
	
	@Before
	public void inicializacao() {
		criarContaContabil();
		inserirVariasMovimentacoesContabilParaListagem();
		recuperarEstatisciaLancamentos();
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.basePath = ConstantesResourceAPI.VERSION_V1 + ConstantesResourceAPI.ENDPOINT_API + BASEPATH;
		RestAssured.port = port;
	}
	
	
	
	@Test
	@Order(1)
	public void deveRealizarLancamentoComSucesso() {
		
		LancamentoContabilRequestDTO request = new LancamentoContabilRequestDTO();
		request.setContaContabil(contaContabilExistente.getContaContabil());
		request.setDataLancamento(LocalDate.now());
		request.setValorLancamento(BigDecimal.valueOf(11545.50).setScale(2));
		
		LancamentoContaContabilCadastroResponse response = RestAssured
				.given()
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(request)
				.log().all()
				.when()
					.post()
				.then()
					.statusCode(HttpStatus.CREATED.value())
					.log().all()
				.extract()
				.response().as(LancamentoContaContabilCadastroResponse.class);
		
		assertNotNull(response.getId());
		aguardarProcessamentoSQS(3000);
		LancamentoContabil lancamento = lancamentoRepository.findByCodigo(response.getId());
		assertNotNull(lancamento);
		assertEquals(lancamento.getValorLancamento(), request.getValorLancamento());
		assertEquals(lancamento.getDataLancamento(), request.getDataLancamento());
		assertEquals(lancamento.getContaContabil(), request.getContaContabil());
	}
	
	
	@Test
	@Order(2)
	public void realizaLancamentoSemContaContabilTest() {
		
		LancamentoContabilRequestDTO request = new LancamentoContabilRequestDTO();
		Problem response = RestAssured
				.given()
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(request)
				.log().all()
				.when()
					.post()
				.then()
					.statusCode(HttpStatus.BAD_REQUEST.value())
					.log().all()
				.extract()
				.response().as(Problem.class);
		
		assertNotNull(response);
		assertEquals(response.getType(), ProblemType.DADOS_INVALIDOS.getUri());
		assertEquals(response.getTitle(), ProblemType.DADOS_INVALIDOS.getTitle());
		
		List<Problem.Object> erros = response.getObjects();
		List<String> listaErros = Lists.list(ConstantesTeste.CONTACONTABIL, ConstantesTeste.DATALANCAMENTO, ConstantesTeste.VALORLANCAMENTO);
		List<String> nameErrosResponse = erros.stream().map(e -> e.getName()).collect(Collectors.toList());
		assertTrue(nameErrosResponse.containsAll(listaErros));
		
	}
	
	@Test
	@Order(3)
	public void deveTentarRealizarLancamentoComCaractereNoCampoValorLancamento () {
		String valorInvalido = "15.20asdad";
		String json = String.format("{\n" + 
				"  \"contaContabil\": 44651,\n" + 
				"  \"dataLancamento\": \"20200620\",\n" + 
				"  \"valorLancamento\": \"%s\"\n" + 
				"}", valorInvalido);
		
		Problem response = RestAssured.given()
					.accept(ContentType.JSON)
					.contentType(ContentType.JSON)
					.body(json)
					.log().all()
					.when()
						.post()
					.then()
						.statusCode(HttpStatus.BAD_REQUEST.value())
						.log().all()
					.extract()
						.response()
						.as(Problem.class);
		
		assertEquals(response.getType(), ProblemType.MENSAGEM_INCOMPREENSIVEL.getUri());
		assertEquals(response.getTitle(), ProblemType.MENSAGEM_INCOMPREENSIVEL.getTitle());
		assertEquals(response.getDetail(), String.format(ConstantesTeste.MENSAGEMINCOMPREENSIVEL, ConstantesTeste.VALORLANCAMENTO, valorInvalido, BigDecimal.class.getSimpleName()));
	}
	
	
	@Test
	@Order(4)
	public void testeLancamentoComContaComCaractereNaoNumerico () {
		String valorInvalido = "44651sdasd";
		String json = String.format("{\n" + 
				"  \"contaContabil\": $s,\n" + 
				"  \"dataLancamento\": \"20200620\",\n" + 
				"  \"valorLancamento\": \"15.20\"\n" + 
				"}", valorInvalido);
		
		Problem response = RestAssured.given()
					.accept(ContentType.JSON)
					.contentType(ContentType.JSON)
					.body(json)
					.log().all()
					.when()
						.post()
					.then()
						.statusCode(HttpStatus.BAD_REQUEST.value())
						.log().all()
					.extract()
						.response()
						.as(Problem.class);
		
		assertEquals(response.getType(), ProblemType.DADOS_INVALIDOS.getUri());
		assertEquals(response.getTitle(), ProblemType.DADOS_INVALIDOS.getTitle());
		assertEquals(response.getDetail(), String.format(ConstantesTeste.DETAIL_DADOS_INVALIDOS_CONTA_INVALIDA, ConstantesTeste.CONTACONTABIL));
	}
	
	@Test
	public void deveListarTodosLancamentosComSucesso() {
		List<LancamentoContaContabilResponse> response = RestAssured.given()
				   .accept(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get()
				   		.jsonPath()
				   			.getList(".", LancamentoContaContabilResponse.class);
				   
		assertNotNull(response);
		assertEquals(response.size(), lancamentos.size());
	}
	
	@Test
	@Order(6)
	public void deveListarLancamentoDeContaQueNaoPossuiLancamentos() {
		List<LancamentoContaContabilResponse> response = RestAssured.given()
				   .queryParam(ConstantesTeste.CONTACONTABIL, 46546545)
				   .accept(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get()
				   		.jsonPath()
				   			.getList(".", LancamentoContaContabilResponse.class);
		assertNotNull(response);
		assertEquals(response.size(), 0);
	}
	
	@Test
	@Order(7)
	public void deveObterLancamentoPorIdComSucesso() {
		 String codigo = lancamentos.get(0).getCodigo();
		 LancamentoContaContabilResponse response = RestAssured.given()
		 		   .pathParam("id", codigo)
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get("/{id}")
				   	.then()
				   		.statusCode(HttpStatus.OK.value())
				   		.log().all()
				   	.and()
				   		.extract().as(LancamentoContaContabilResponse.class);
		 
		assertNotNull(response);
		assertEquals(response.getCodigo(), lancamentos.get(0).getCodigo());
		assertEquals(response.getValor().setScale(2), lancamentos.get(0).getValorLancamento().setScale(2));
		assertEquals(response.getDataLancamento(), lancamentos.get(0).getDataLancamento());
	}
	
	@Test
	@Order(7)
	public void deveFalharAoObterLancamentoComCodigoQueNaoExiste() {
		String codigo = Util.gerarUUID();
		Problem response = RestAssured.given()
		 		   .pathParam("id", codigo)
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get("/{id}")
				   	.then()
				   		.statusCode(HttpStatus.NOT_FOUND.value())
				   		.log().all()
				   	.and()
				   		.extract().as(Problem.class);
		 
		assertNotNull(response);
		assertEquals(response.getType(), ProblemType.RECURSO_NAO_ENCONTRADO.getUri());
		assertEquals(response.getTitle(), ProblemType.RECURSO_NAO_ENCONTRADO.getTitle());
		assertEquals(response.getDetail(), String.format(ConstantesTeste.DETAIL_LANCAMENTO_INEXISTENTE, codigo));
	}
	
	@Test
	@Order(7)
	public void deveFalharAoUsarUmVerboIncorreto() {
		String codigo = Util.gerarUUID();
		Problem response = RestAssured.given()
		 		   .pathParam("id", codigo)
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.put("/{id}")
				   	.then()
				   		.statusCode(HttpStatus.METHOD_NOT_ALLOWED.value())
				   		.log().all()
				   	.and()
				   		.extract().as(Problem.class);
		
		assertNotNull(response);
		assertEquals(response.getType(), ProblemType.VERBO_NAO_PERMITIDO.getUri());
		assertEquals(response.getTitle(), ProblemType.VERBO_NAO_PERMITIDO.getTitle());
		assertEquals(response.getDetail(), String.format(ConstantesTeste.DETAIL_VERBO_NAO_ACEITO, HttpMethod.POST));
	}
	
	@Test
	@Order(8)
	public void deveFalharAoRealizarRequestParaURLNaoMapeada() {
		String codigo = Util.gerarUUID();
		Problem response = RestAssured.given()
		 		   .pathParam("id", codigo)
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.put("/contabilidade/{id}")
				   	.then()
				   		.statusCode(HttpStatus.NOT_FOUND.value())
				   		.log().all()
				   	.and()
				   		.extract().as(Problem.class);
		
		assertNotNull(response);
		assertEquals(response.getType(), ProblemType.RECURSO_NAO_ENCONTRADO.getUri());
		assertEquals(response.getTitle(), ProblemType.RECURSO_NAO_ENCONTRADO.getTitle());
		assertEquals(response.getDetail(),ConstantesTeste.DETAIL_RECURSO_NAO_ENCONTRADO);
	}
	
	@Test
	@Order(9)
	public void deveRecuperarEstatisticasComSucesso() {
		ResultadoEstatistica response = RestAssured.given()
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get("/stats")
				   	.then()
				   		.statusCode(HttpStatus.OK.value())
				   		.log().all()
				   	.and()
				   		.extract().as(ResultadoEstatistica.class);
		
		assertNotNull(response);
		assertEquals(response.getSoma().setScale(2), resultadoEstatistica.getSoma().setScale(2));
		assertEquals(response.getMedia().setScale(2), resultadoEstatistica.getMedia().setScale(2));
		assertEquals(response.getMaximo().setScale(2), resultadoEstatistica.getMaximo().setScale(2));
		assertEquals(response.getMinimo().setScale(2), resultadoEstatistica.getMinimo().setScale(2));
		assertEquals(response.getQuantidade(), resultadoEstatistica.getQuantidade());
		
	}
	
	@Test
	@Order(9)
	public void deveRecuperarEstatisticasDeContaQueNaoExiste() {
		ResultadoEstatistica response = RestAssured.given()
				   .queryParam("contaContabil", 1234)
				   .accept(ContentType.JSON)
				   .contentType(ContentType.JSON)
				   .log().all()
				   .when()
				   		.get("/stats")
				   	.then()
				   		.statusCode(HttpStatus.OK.value())
				   		.log().all()
				   	.and()
				   		.extract().as(ResultadoEstatistica.class);
		
		assertNotNull(response);
		assertEquals(response.getSoma(), BigDecimal.valueOf(0).setScale(2));
		assertEquals(response.getMedia(), BigDecimal.valueOf(0).setScale(2));
		assertEquals(response.getMaximo(), BigDecimal.valueOf(0).setScale(2));
		assertEquals(response.getMinimo(), BigDecimal.valueOf(0).setScale(2));
		assertEquals(response.getQuantidade(), Long.valueOf(0));
		
	}
	
	private void criarContaContabil() {
		ContaContabil conta = new ContaContabil();
		conta.setContaContabil(NUMEROCONTACONTABILCRIAR);
		contaContabilExistente = contaContabilRepository.save(conta);
	}
	
	private void recuperarEstatisciaLancamentos() {
		BigDecimal soma = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).sum()).setScale(2,RoundingMode.HALF_UP);
		BigDecimal media = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).average().orElse(0.0)).setScale(2,RoundingMode.HALF_UP);
		BigDecimal minimo = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).min().orElse(0.0)).setScale(2,RoundingMode.HALF_UP);
		BigDecimal maximo = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).max().orElse(0.0)).setScale(2).setScale(2,RoundingMode.HALF_UP);
		Long quantidade = lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).count();
		
		resultadoEstatistica = new ResultadoEstatistica(soma, maximo, minimo, media, quantidade);
	}

	private void inserirVariasMovimentacoesContabilParaListagem() {
		
		LancamentoContabil contabil = new LancamentoContabil();
		contabil.setContaContabil(contaContabilExistente.getContaContabil());
		contabil.setDataLancamento(LocalDate.now().minusDays(2));
		contabil.setCodigo(Util.gerarUUID());
		contabil.setStatusLancamento(StatusLancamento.Lancado);
		contabil.setValorLancamento(BigDecimal.valueOf(2502.40).setScale(2));
		
		LancamentoContabil contabil1 = new LancamentoContabil();
		contabil1.setContaContabil(contaContabilExistente.getContaContabil());
		contabil1.setDataLancamento(LocalDate.now().minusDays(3));
		contabil1.setCodigo(Util.gerarUUID());
		contabil1.setStatusLancamento(StatusLancamento.Lancado);
		contabil1.setValorLancamento(BigDecimal.valueOf(25402.28).setScale(2));
		
		LancamentoContabil contabil2 = new LancamentoContabil();
		contabil2.setContaContabil(contaContabilExistente.getContaContabil());
		contabil2.setDataLancamento(LocalDate.now());
		contabil2.setCodigo(Util.gerarUUID());
		contabil2.setStatusLancamento(StatusLancamento.Lancado);
		contabil2.setValorLancamento(BigDecimal.valueOf(25402.28).setScale(2));
		
		lancamentos = Lists.list(contabil, contabil1, contabil2);
		lancamentos = lancamentoRepository.saveAll(lancamentos);
		
	}
	
	private void aguardarProcessamentoSQS(Integer time) {
		try {
			Thread.sleep(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void finalize() {
		cleanDataBase();
	}

	private void cleanDataBase() {
		lancamentoRepository.deleteAllInBatch();
		contaContabilRepository.deleteAllInBatch();
	}
}
