package br.com.digio.api.resources;

public class ConstantesTeste {
	
	public static final String CONTACONTABIL = "contaContabil";
	public static final String VALORLANCAMENTO = "valorLancamento";
	public static final String DATALANCAMENTO= "dataLancamento";
	public static final String MENSAGEMINCOMPREENSIVEL = 
	"A propriedade '%s' recebeu o valor '%s' que é um tipo inválido. Corrija e informe "
	+ "um valor do tipo %s";
	public static final String DETAIL_DADOS_INVALIDOS_CONTA_INVALIDA =  "A propriedade %s está incorreta. Verifique o valor informado e tente novamente.";
	public static final String DETAIL_LANCAMENTO_INEXISTENTE = "O lancamento de codigo %s não foi encontrado";
	public static final String DETAIL_VERBO_NAO_ACEITO = "Verbo HTTP %s não é aceito para este endpoint";
	public static final String DETAIL_RECURSO_NAO_ENCONTRADO = "O recurso buscado não foi encontrado";
}
