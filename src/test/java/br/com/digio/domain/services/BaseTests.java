package br.com.digio.domain.services;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.infraestructure.repository.ContaContabilRepository;
@Component
public class BaseTests {

	
	@Autowired
	private ContaContabilRepository contaRepository;
	@PostConstruct
	public void init() {
	}
	
}
