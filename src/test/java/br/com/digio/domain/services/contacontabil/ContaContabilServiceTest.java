package br.com.digio.domain.services.contacontabil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.domain.exception.ContaContabilInexistenteException;
import br.com.digio.infraestructure.repository.ContaContabilRepository;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test.properties")
@EnableJpaRepositories(basePackages = "br.com.digio.infraestructure.repository")
public class ContaContabilServiceTest {
	
	private static final Long NUMEROCONTACONTABILCRIAR = 44921L;
	
	private static final Long NUMEROCONTACONTABILINEXISTENTE = 412L;

	@Autowired
	private ContaContabilRepository contaContabilRepository;
	
	@Autowired
	private ContaContabilService contaContabilService;
	
	@PostConstruct
	public void init() {
		criarContaContabil();
	}
	
	private void criarContaContabil() {
		ContaContabil conta = new ContaContabil();
		conta.setContaContabil(NUMEROCONTACONTABILCRIAR);
		contaContabilRepository.save(conta);
	}
	
	
	@Test
	public void deveBuscarComSucesso() throws Exception {
		ContaContabil contaBuscada = contaContabilService.buscar(NUMEROCONTACONTABILCRIAR);
		assertNotNull(contaBuscada);
		assertEquals(contaBuscada.getContaContabil(), NUMEROCONTACONTABILCRIAR);
	}

	@Test(expected = ContaContabilInexistenteException.class)
	public void deveRetornarContaInexistente() throws Exception  {
		contaContabilService.buscar(NUMEROCONTACONTABILINEXISTENTE);
	}

	
	@PreDestroy
	public void destroy() throws SQLException {
		limparBase();
	}
	
	private void limparBase() {
		List<ContaContabil> contaDeletar = contaContabilRepository.findByContaContabil(NUMEROCONTACONTABILCRIAR);
		contaContabilRepository.deleteInBatch(contaDeletar);
	}
	
}
