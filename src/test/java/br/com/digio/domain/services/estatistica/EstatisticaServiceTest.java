package br.com.digio.domain.services.estatistica;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource("/application-test.properties")
@EnableJpaRepositories(basePackages = "br.com.digio.infraestructure.repository")
public class EstatisticaServiceTest {
	
	public EstatisticaServiceTest() {
		
	}
	@Test
	public void test() {
		
	}

}
