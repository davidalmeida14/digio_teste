package br.com.digio.domain.services.movimentacaocontabil;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.model.StatusLancamento;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.request.LancamentoContabilSearch;
import br.com.digio.domain.exception.LancamentoExistenteException;
import br.com.digio.domain.exception.LancamentoNaoEncontradoException;
import br.com.digio.domain.services.estatistica.EstatisticaService;
import br.com.digio.domain.services.sqs.SQSLancamentoService;
import br.com.digio.infraestructure.repository.ContaContabilRepository;
import br.com.digio.infraestructure.repository.LancamentoContabilRepository;
import br.com.digio.utils.Util;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author davidalmeida
 *
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("/application-test.properties")
@EnableJpaRepositories(basePackages = "br.com.digio.infraestructure.repository")
@Slf4j
public class MovimentacaoContabilServiceTest {

	private static final Long NUMEROCONTACONTABILCRIAR = 44921L;

	@Autowired
	private MovimentacaoContabilService movimentacaoService;

	@Autowired
	private LancamentoContabilRepository lancamentoRepository;
	
	@Autowired
	private ContaContabilRepository contaContabilRepository;
	
	private ContaContabil contaContabilExistente;

	private LancamentoContabil lancamentoInserido;
	
	private ResultadoEstatistica resultadoEstatistica;
	
	@Autowired
	private EstatisticaService estatisticaService;
	
	@Autowired SQSLancamentoService sqsLancamentoService;
	
	@Autowired ExecutorService service;
	
	List<LancamentoContabil> lancamentos = Lists.newArrayList();

	@Before
	public void onSetUp() {
		limparBase();
		criarContaContabil();
		inserirVariasMovimentacoesContabilParaListagem();
		inserirMovimentacaoContabil();
		recuperarEstatisciaLancamentos();
	}

	private void recuperarEstatisciaLancamentos() {
		BigDecimal soma = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).sum()).setScale(2,RoundingMode.HALF_UP);
		BigDecimal media = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).average().orElse(0.0)).setScale(2,RoundingMode.HALF_UP);
		BigDecimal minimo = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).min().orElse(0.0)).setScale(2,RoundingMode.HALF_UP);
		BigDecimal maximo = BigDecimal.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).max().orElse(0.0)).setScale(2).setScale(2,RoundingMode.HALF_UP);
		Long quantidade = lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).count();
		
		resultadoEstatistica = new ResultadoEstatistica(soma, maximo, minimo, media, quantidade);
	}

	private void inserirVariasMovimentacoesContabilParaListagem() {
		
		LancamentoContabil contabil = new LancamentoContabil();
		contabil.setContaContabil(contaContabilExistente.getContaContabil());
		contabil.setDataLancamento(LocalDate.now().minusDays(2));
		contabil.setCodigo(Util.gerarUUID());
		contabil.setStatusLancamento(StatusLancamento.Lancado);
		contabil.setValorLancamento(BigDecimal.valueOf(2502.40).setScale(2));
		
		LancamentoContabil contabil1 = new LancamentoContabil();
		contabil1.setContaContabil(contaContabilExistente.getContaContabil());
		contabil1.setDataLancamento(LocalDate.now().minusDays(3));
		contabil1.setCodigo(Util.gerarUUID());
		contabil1.setStatusLancamento(StatusLancamento.Lancado);
		contabil1.setValorLancamento(BigDecimal.valueOf(25402.28).setScale(2));
		
		LancamentoContabil contabil2 = new LancamentoContabil();
		contabil2.setContaContabil(contaContabilExistente.getContaContabil());
		contabil2.setDataLancamento(LocalDate.now());
		contabil2.setCodigo(Util.gerarUUID());
		contabil2.setStatusLancamento(StatusLancamento.Lancado);
		contabil2.setValorLancamento(BigDecimal.valueOf(25402.28).setScale(2));
		
		lancamentos = Lists.list(contabil, contabil1, contabil2);
		lancamentos = lancamentoRepository.saveAll(lancamentos);
		
	}

	private void inserirMovimentacaoContabil() {
		LancamentoContabil contabil = new LancamentoContabil();
		contabil.setContaContabil(contaContabilExistente.getContaContabil());
		contabil.setDataLancamento(LocalDate.now().minusDays(2));
		contabil.setCodigo(Util.gerarUUID());
		contabil.setStatusLancamento(StatusLancamento.Lancado);
		contabil.setValorLancamento(BigDecimal.valueOf(2502.40).setScale(2));
		lancamentoInserido = lancamentoRepository.save(contabil);
		lancamentos.add(lancamentoInserido);
	}

	private void criarContaContabil() {
		ContaContabil conta = new ContaContabil();
		conta.setContaContabil(NUMEROCONTACONTABILCRIAR);
		contaContabilExistente = contaContabilRepository.save(conta);
	}

	@Test
	public void deveLancarMovimentacaoComSucessoTest() throws Exception {
		LancamentoContabilRequestDTO request = new LancamentoContabilRequestDTO();
		LocalDate dataLancamento = LocalDate.now();
		request.setContaContabil(NUMEROCONTACONTABILCRIAR);
		request.setDataLancamento(dataLancamento);
		request.setValorLancamento(BigDecimal.valueOf(254021.92));
		
		LancamentoContabil lancamento = movimentacaoService.lancar(request);
		log.info("Lançamento : {} realizado: {} ", lancamento.getCodigo(), LocalDateTime.now());
		aguardarProcessamentoSQS(3000);
		log.info("Delay de processamento realizado");
		LancamentoContabil lancamentoBuscado = lancamentoRepository.findByCodigo(lancamento.getCodigo());
		log.info("Lancamento: {} buscado", lancamentoBuscado);
		assertEquals(lancamentoBuscado.getCodigo(), lancamento.getCodigo());
		assertEquals(lancamentoBuscado.getContaContabil(), lancamento.getContaContabil());
		assertEquals(lancamentoBuscado.getValorLancamento(), lancamento.getValorLancamento());
		
	}
	
	
	@Test(expected = LancamentoExistenteException.class)
	public void deveValidarLancamentoExistente() throws Exception {
		List<ContaContabil> conta = contaContabilRepository.findByContaContabil(lancamentoInserido.getContaContabil());
		movimentacaoService.validaLancamentoExistente(conta.get(0), lancamentoInserido.getDataLancamento(), lancamentoInserido.getValorLancamento());
	}
	
	
	@Test(expected = LancamentoExistenteException.class)
	public void deveLancarExcecaoDeDuplicdadeDeLancamento() throws Exception {
		LancamentoContabilRequestDTO lancamento = new LancamentoContabilRequestDTO();
		lancamento.setContaContabil(lancamentoInserido.getContaContabil());
		lancamento.setDataLancamento(lancamentoInserido.getDataLancamento());
		lancamento.setValorLancamento(lancamentoInserido.getValorLancamento());
		movimentacaoService.lancar(lancamento);
	}
	
	@Test
	public void deveBucarPeloCodigo() throws Exception {
		LancamentoContabil lancamento = movimentacaoService.buscar(lancamentoInserido.getCodigo());
		assertNotNull(lancamento);
		assertEquals(lancamento.getCodigo(), lancamentoInserido.getCodigo());
		assertEquals(lancamento.getContaContabil(), lancamentoInserido.getContaContabil());
		assertEquals(lancamento.getValorLancamento(), lancamentoInserido.getValorLancamento());
	}
	
	@Test(expected = LancamentoNaoEncontradoException.class)
	public void deveFalharBuscaComCodigoInvalido() throws Exception {
		movimentacaoService.buscar(UUID.randomUUID().toString());
	}

	@Test
	public void deveListarTodosLancamentos() {
		Integer quantidadeRegistrosMatch = 0;
		LancamentoContabilSearch search = new LancamentoContabilSearch();
		search.setStatusLancamento(StatusLancamento.Lancado);
		List<LancamentoContabil> lancamentosRetornados = movimentacaoService.listar(search);
		assertNotNull(lancamentosRetornados);
		/* 
		 * Existem registros inseridos fora do contexto desse teste. 
		 * Portanto, está sendo validado apenas os que foram inseridos previamente para esse case
		 */
		for (LancamentoContabil lancamentoContabil : lancamentosRetornados) {
			for (LancamentoContabil lancamentoInserido : lancamentos) {
				if(lancamentoContabil.getCodigo().equals(lancamentoInserido.getCodigo())) {
					quantidadeRegistrosMatch = quantidadeRegistrosMatch + 1 ;
					assertEquals(lancamentoContabil.getContaContabil(), lancamentoInserido.getContaContabil());
					assertEquals(lancamentoContabil.getDataLancamento(), lancamentoInserido.getDataLancamento());
					assertEquals(lancamentoContabil.getValorLancamento(), lancamentoInserido.getValorLancamento());
				}
			}
		}
		assertThat(quantidadeRegistrosMatch == lancamentos.size());
	}
	
	
	
	@Test
	public void deveRecuperarEstatisticasComSucesso() throws Exception {
		LancamentoContabil contabil = new LancamentoContabil();
		ResultadoEstatistica estatistica = estatisticaService.obterEstatisticas(contabil);
		assertNotNull(estatistica);
		assertEquals(estatistica.getMaximo(), resultadoEstatistica.getMaximo());
		assertEquals(estatistica.getMinimo(), resultadoEstatistica.getMinimo());
		assertEquals(estatistica.getSoma(), resultadoEstatistica.getSoma());
		assertEquals(estatistica.getQuantidade(), resultadoEstatistica.getQuantidade());
	}
	
	private void aguardarProcessamentoSQS(Integer time) {
		try {
			Thread.sleep(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void onTearDown() {
		limparBase();
	}

	private void limparBase() {
		contaContabilRepository.deleteAllInBatch();
		lancamentoRepository.deleteAllInBatch();
	}
}
