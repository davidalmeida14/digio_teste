package br.com.digio.infraestructure.configuration;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;

import br.com.digio.utils.Util;

@Configuration
public class APIConfiguration {

	@Value("${numeroThreads}")
	private Integer numeroThreads;
	
	@Bean
	public ExecutorService executors() {
		ExecutorService service = Executors.newFixedThreadPool(numeroThreads);
		return service;
	}
	
	public static void main(String[] args) {
		List<String> ids = Lists.newArrayList();
		for(int i = 0; i < 3; i ++) {
			ids.add(Util.gerarUUID());	
		}
		
		ids.forEach(s -> System.out.println(ids));
	}
	
}
