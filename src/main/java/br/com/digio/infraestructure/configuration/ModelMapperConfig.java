package br.com.digio.infraestructure.configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.response.LancamentoContaContabilCadastroResponse;


@Configuration
public class ModelMapperConfig {

	TypeMap<LancamentoContabil, LancamentoContaContabilCadastroResponse> typeMap = null;
	TypeMap<LancamentoContabilRequestDTO, LancamentoContabil> typeMapData = null;

	@Bean
	@RequestScope
	public ModelMapper modelMapper() {

		ModelMapper modelMapper = new ModelMapper();

		Converter<String, LocalDate> toLocaldate = new AbstractConverter<String, LocalDate>() {
			@Override
			protected LocalDate convert(String data) {
				return convertStringToLocalDateTimeAtEndDay(data, "yyyyMMdd").toLocalDate();
			}
		};

		modelMapper.createTypeMap(String.class, LocalDate.class);
		modelMapper.addConverter(toLocaldate);
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelMapper;

	}
	
	 /**
     * Converte uma data em String de um padrão para outro padrão
     * @param date
     * @param patternOrigem 
     * @param patternDestino 
     * @return
     */
    public static String convertStringDateToStringFromPattern(String date, String patternOrigem, String patternDestino) {
    	
    	String dataFormatada = null;
    	SimpleDateFormat formatterOrigem = new SimpleDateFormat(patternOrigem);
    	DateTimeFormatter formatterDestino = DateTimeFormatter.ofPattern(patternDestino);
    	
    	try {
			Date dataParseada = formatterOrigem.parse(date);
			dataFormatada = LocalDate.ofInstant(dataParseada.toInstant(), ZoneId.systemDefault()).format(formatterDestino);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return dataFormatada;
    	
    }
    
    public static LocalDate convertString(String date) {
        return LocalDate.parse(date.toString());
    }

    
    /**
     * Converte uma String em LocalDateTime (final do dia) a partir de um pattern
     * 
     * @param date
     * @param pattern
     * @return
     */
    public static LocalDateTime convertStringToLocalDateTimeAtEndDay(String date, String pattern) {
    	
    	if(StringUtils.isNotBlank(date) && StringUtils.isNotBlank(pattern)) {
    		
    		try {
    			LocalDate data = convertString(date);
            	
            	LocalDateTime time = convertDataFinal(data);
            	
            	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            	
            	LocalDateTime parse = LocalDateTime.parse(time.format(formatter), formatter);
            	
            	return parse;
    		} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
        	
    	} else {
    		return null;
    	}
    	    	
    }
    
    public static LocalDateTime convertDataFinal(LocalDate date) {
        return LocalDateTime.parse(date.toString() + "T00:00:00.000");
    }



}
