package br.com.digio.infraestructure.configuration.sqs;

import javax.jms.Session;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

@Configuration
@EnableJms
public class SQSConfiguration {

	@Value("${amazon.accessKey}")
	private String accessKey;
	@Value("${amazon.secretKey}")
	private String secretKey;

//	@Autowired
//	private SQSConnectionFactory connectionFactory;

	@SuppressWarnings("deprecation")
	@Bean
	public SQSConnectionFactory createConnectionFactory() {
		
		return SQSConnectionFactory.builder()
									.withRegion(Region.getRegion(Regions.SA_EAST_1))
									.withAWSCredentialsProvider(
											new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
									.build();
	}

	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(
			@Lazy SQSConnectionFactory connectionFactory) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setDestinationResolver(new DynamicDestinationResolver());
		factory.setSessionAcknowledgeMode(Session.AUTO_ACKNOWLEDGE);
		return factory;
	}

	@Bean
	public JmsTemplate template(@Lazy SQSConnectionFactory factory) {
		return new JmsTemplate(factory);
	}

//	@PostConstruct
//	public void listar() {
//		SendMessageResult sendMessage = AmazonSQSClientBuilder
//				.defaultClient()
//				.sendMessage(new SendMessageRequest("https://sqs.sa-east-1.amazonaws.com/142994317638/awstestqueue", "Envio mensagem teste")).;
//		sendMessage.getMessageId();
//		AmazonSQS build = AmazonSQSClient.builder()
//				.withRegion(Regions.SA_EAST_1)
//				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
//				.build();
//		ListQueuesResult listQueues = build.listQueues();
//		listQueues.getQueueUrls().forEach(queue -> System.out.println("Fila: " + queue));
//
//	}

}
