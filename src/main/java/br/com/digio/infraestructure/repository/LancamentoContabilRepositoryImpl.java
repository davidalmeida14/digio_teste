package br.com.digio.infraestructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zaxxer.hikari.HikariDataSource;

import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.domain.exception.RegistrosEstatisticasInexistentes;

@Component
public class LancamentoContabilRepositoryImpl {

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private HikariDataSource dataSource;
	public String query = "SELECT \n" + 
			"COUNT(*) AS QUANTIDADE,\n" + 
			"SUM(VALORLANCAMENTO) AS SOMA,\n" + 
			"MAX(VALORLANCAMENTO) AS MAXIMO,\n" + 
			"MIN(VALORLANCAMENTO) AS MINIMO,\n" + 
			"AVG(VALORLANCAMENTO) as MEDIA\n" + 
			"FROM LANCAMENTOCONTABIL";
	
	public String queryIdConta =
			"SELECT \n" + 
			"COUNT(*) AS QUANTIDADE,\n" + 
			"SUM(VALORLANCAMENTO) AS SOMA,\n" + 
			"MAX(VALORLANCAMENTO) AS MAXIMO,\n" + 
			"MIN(VALORLANCAMENTO) AS MINIMO,\n" + 
			"AVG(VALORLANCAMENTO) as MEDIA\n" + 
			"FROM LANCAMENTOCONTABIL WHERE CONTACONTABIL = ?";
	
	public ResultadoEstatistica listarEstatistica() {
		Connection connection = null;
		try {
			
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet resultSet = statement.executeQuery();
			ResultadoEstatistica estatistica = new ResultadoEstatistica();
			while(resultSet.next()) {
				estatistica.setQuantidade(resultSet.getLong("QUANTIDADE"));
				estatistica.setSoma(resultSet.getBigDecimal("SOMA"));
				estatistica.setMaximo(resultSet.getBigDecimal("MAXIMO"));
				estatistica.setMinimo(resultSet.getBigDecimal("MINIMO"));
				estatistica.setMedia(resultSet.getBigDecimal("MEDIA"));
			}
			return estatistica;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(Objects.nonNull(connection) && !connection.isClosed()) {
					connection.close();
				} 
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	};
	
	public ResultadoEstatistica listarEstatistica(Long contaContabil) throws RegistrosEstatisticasInexistentes {
		
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(queryIdConta);
			statement.setLong(1, contaContabil);
			ResultSet resultSet = statement.executeQuery();
			ResultadoEstatistica estatistica = new ResultadoEstatistica();
			while(resultSet.next()) {
				estatistica.setQuantidade(resultSet.getLong("QUANTIDADE"));
				estatistica.setSoma(resultSet.getBigDecimal("SOMA"));
				estatistica.setMaximo(resultSet.getBigDecimal("MAXIMO"));
				estatistica.setMinimo(resultSet.getBigDecimal("MINIMO"));
				estatistica.setMedia(resultSet.getBigDecimal("MEDIA"));
			}
			if(estatistica.getQuantidade().compareTo(0L) == 0) {
				throw new RegistrosEstatisticasInexistentes();
			}
			return estatistica;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(Objects.nonNull(connection) && !connection.isClosed()) {
					connection.close();
				} 
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
		
	};
	
	
}
