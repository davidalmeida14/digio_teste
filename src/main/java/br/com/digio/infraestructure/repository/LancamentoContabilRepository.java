package br.com.digio.infraestructure.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.digio.api.model.LancamentoContabil;

@Repository
public interface LancamentoContabilRepository extends JpaRepository<LancamentoContabil, Long> {
	
	//LancamentoContabil findByContaContabilAndDataLancamentoAndValorLancamento(Long contaContabil, LocalDate dataLancamento, BigDecimal valor);
	List<LancamentoContabil> findByContaContabilAndDataLancamentoAndValorLancamento(Long contaContabil, LocalDate dataLancamento, BigDecimal valor);
	LancamentoContabil findByCodigo(String codigo);
}