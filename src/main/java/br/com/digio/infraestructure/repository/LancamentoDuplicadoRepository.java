package br.com.digio.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.digio.api.model.LancamentoContabilDuplicado;

@Repository
public interface LancamentoDuplicadoRepository extends JpaRepository<LancamentoContabilDuplicado, Long> {

}
