package br.com.digio.infraestructure.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.digio.api.model.ContaContabil;

@Repository
public interface ContaContabilRepository extends JpaRepository<ContaContabil, Long>{

	List<ContaContabil> findByContaContabil(Long contaContabil);
}
