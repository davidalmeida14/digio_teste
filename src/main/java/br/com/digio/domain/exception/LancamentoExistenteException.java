package br.com.digio.domain.exception;

import static br.com.digio.utils.MensagensErro.LANCAMENTO_EXISTENTE;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 
 * @author davidalmeida
 *
 */
public class LancamentoExistenteException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public LancamentoExistenteException(String message) {
		super(message);
	}
	
	public LancamentoExistenteException(Long contaContabil, LocalDate data, BigDecimal valor) {
		this(String.format(LANCAMENTO_EXISTENTE, contaContabil, data.toString(), valor));
	}

}
