package br.com.digio.domain.exception;
/**
 * 
 * @author davidalmeida
 *
 */
public class ContaContabilInexistenteException extends EntidadeInexistenteException {

	public ContaContabilInexistenteException(String mensagem) {
		super(mensagem);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -8663624552216009419L;

}
