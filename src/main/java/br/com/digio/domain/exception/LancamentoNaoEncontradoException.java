package br.com.digio.domain.exception;

import br.com.digio.utils.MensagensErro;
/**
 * 
 * @author davidalmeida
 *
 */
public class LancamentoNaoEncontradoException extends EntidadeInexistenteException{

	private static final long serialVersionUID = -9030789644997472170L;
	
	public LancamentoNaoEncontradoException(String message) {
		super(String.format(MensagensErro.LANCAMENTO_NAO_ENCONTRADO, message));
	}

}
