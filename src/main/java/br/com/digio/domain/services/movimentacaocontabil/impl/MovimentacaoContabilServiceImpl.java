package br.com.digio.domain.services.movimentacaocontabil.impl;

import static br.com.digio.utils.Util.checkThrow;
import static br.com.digio.utils.Util.gerarUUID;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.digio.api.core.CallableSQS;
import br.com.digio.api.model.ContaContabil;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.request.LancamentoContabilSearch;
import br.com.digio.domain.exception.LancamentoExistenteException;
import br.com.digio.domain.exception.LancamentoNaoEncontradoException;
import br.com.digio.domain.services.contacontabil.ContaContabilService;
import br.com.digio.domain.services.movimentacaocontabil.MovimentacaoContabilService;
import br.com.digio.domain.services.sqs.SQSLancamentoService;
import br.com.digio.infraestructure.repository.LancamentoContabilRepository;
import br.com.digio.utils.GenericConverter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author davidalmeida
 *
 */
@Service
@Slf4j
public class MovimentacaoContabilServiceImpl implements MovimentacaoContabilService {

	@Autowired
	private LancamentoContabilRepository lancamentoRepository;
	
	@Autowired
	private ContaContabilService contaContabilService;

	@Autowired
	private GenericConverter converter;
	
	@PersistenceContext
	private EntityManager em;
	
	@Value("${amazon.tentativasEnvioSQS}")
	private int	 tentativasEnvioSQS;
	
	@Autowired
	SQSLancamentoService sqsLancamentoService;
	
	@Autowired
	private ExecutorService service;
	
	@Override
	public LancamentoContabil lancar(LancamentoContabilRequestDTO lancamentoContabil) throws Exception {
		LancamentoContabil movimentacao = new LancamentoContabil();
		try {
			movimentacao = converter.convertModelMapper(lancamentoContabil, LancamentoContabil.class);
			movimentacao.setCodigo(gerarUUID());
			lancamentoContabil.setCodigo(movimentacao.getCodigo());
			ContaContabil contaContabilBuscada = contaContabilService.buscar(movimentacao.getContaContabil());
			this.validaLancamentoExistente(contaContabilBuscada, movimentacao.getDataLancamento(), movimentacao.getValorLancamento());
			efetivarLancamento(lancamentoContabil, sqsLancamentoService);
			
		} catch (Exception e) {
			log.error("Erro ao processa lançamento contábil. Erro: {}, Request: {}", e.getMessage(), lancamentoContabil);
			throw e;
		}
		return movimentacao;
	}
	/**
	 * Método para efetivar o lançamento através de uma implementação de ExecutorService
	 * 
	 * @param lancamentoContabil
	 * @param sqsLancamentoService2
	 */
	private void efetivarLancamento(LancamentoContabilRequestDTO lancamentoContabil,
			SQSLancamentoService sqsLancamentoService2) {
		CallableSQS callableSQS = new CallableSQS(lancamentoContabil, sqsLancamentoService);
		service.submit(callableSQS);
	}

	public void validaLancamentoExistente(ContaContabil contaContabilBuscada, LocalDate data, BigDecimal valor) {
		List<LancamentoContabil> lancamentosPorContaValorEData = lancamentoRepository.findByContaContabilAndDataLancamentoAndValorLancamento(contaContabilBuscada.getContaContabil(), data, valor);
		if(lancamentosPorContaValorEData.size() > 0) {
			throw new LancamentoExistenteException(contaContabilBuscada.getContaContabil(), data, valor);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public LancamentoContabil buscar(String codigo) throws Exception {
		LancamentoContabil lancamento = new LancamentoContabil();
		try {
			lancamento = lancamentoRepository.findByCodigo(codigo);
			checkThrow(Objects.isNull(lancamento), new LancamentoNaoEncontradoException(codigo));
		} catch (Exception e) {
			log.error("Erro ao buscar lançamento contábil: {}. Erro: {}", codigo, e.getMessage());
			throw e;
		}
		return lancamento;
	}

	@Override
	@Transactional(readOnly = true)
	public List<LancamentoContabil> listar(LancamentoContabilSearch lancamentoContabilSearch) {
		LancamentoContabil lancamentoContabil = new LancamentoContabil();
		try {
			lancamentoContabil = converter.convertModelMapper(lancamentoContabilSearch, LancamentoContabil.class);
			ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreNullValues();
			Example<LancamentoContabil> example = Example.of(lancamentoContabil, matcher);
			return lancamentoRepository.findAll(example);
		} catch (Exception e) {
			log.error("Erro ao listar lancamentos contábeis. Erro: {}, Request: {}", e.getMessage(), lancamentoContabilSearch);
			throw e;
		}
		
	}

}
