package br.com.digio.domain.services.estatistica;

import org.springframework.stereotype.Service;

import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
/**
 * 
 * @author davidalmeida
 *
 */
@Service
public interface EstatisticaService {
	/**
	 * Método responsável por retornar estatísticas de lançamentos 
	 * baseadas nas informações passadas na requisição
	 * @param lancamento
	 * @return
	 * @throws Exception
	 */
	ResultadoEstatistica obterEstatisticas(LancamentoContabil lancamento) throws Exception;

}
