package br.com.digio.domain.services.contacontabil.impl;

import static br.com.digio.utils.Util.checkThrow;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.domain.exception.ContaContabilInexistenteException;
import br.com.digio.domain.services.contacontabil.ContaContabilService;
import br.com.digio.infraestructure.repository.ContaContabilRepository;
/**
 * 
 * @author davidalmeida
 *
 */
@Service
public class ContaContabilServiceImpl implements ContaContabilService {
	
	
	@Autowired
	private ContaContabilRepository contaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public ContaContabil buscar(Long numeroContaContabil) throws Exception {
		List<ContaContabil> contaContabil = contaRepository.findByContaContabil(numeroContaContabil);
		checkThrow(contaContabil.isEmpty(), new ContaContabilInexistenteException("ContaContabilInexistente"));
		return contaContabil.get(0);
	}


}
