package br.com.digio.domain.services.sqs;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.model.LancamentoContabilDuplicado;
import br.com.digio.api.model.StatusLancamento;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.domain.services.contacontabil.ContaContabilService;
import br.com.digio.domain.services.movimentacaocontabil.MovimentacaoContabilService;
import br.com.digio.infraestructure.repository.LancamentoContabilRepository;
import br.com.digio.infraestructure.repository.LancamentoDuplicadoRepository;
import br.com.digio.utils.Constantes;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author davidalmeida
 *
 */
@Service
@Slf4j
public class SQSLancamentoService implements SQSService {
	
	@Autowired
	private JmsTemplate template;
	
	@Autowired
	private LancamentoContabilRepository lancamentoRepository;
	
	@Autowired
	private MovimentacaoContabilService movimentacaoService;
	
	@Autowired
	private ContaContabilService contaContabilService;
	
	@Autowired 
	ContaContabilService contaService;
	
	@Autowired
	private LancamentoDuplicadoRepository lancamentoDuplicadoRepository;
	
	Integer tentativas = 0;
	
	@Override
	public void sendMessage(String queue, String message, Integer tentativasEnvio) {
		try {
			template.convertAndSend(queue, message);
		}catch (JmsException e) {
			log.error("Erro ao enviar mensagem a fila do SQS: {}", e.getMessage());
			if(tentativas < tentativasEnvio) {
				tentativas = tentativas + 1;
				sendMessage(queue, message, tentativasEnvio);
				if(tentativas == tentativasEnvio) {
					try {
						salvarLancamentoContingencia(message);
					} catch (Exception ex) {
						log.error("Não foi possível salvar o lançamento em contigência. Erro: {}", ex.getMessage());
					}
				}
				
			} 
		}
		
	}
	
	@Override
	@JmsListener(destination = Constantes.LANCAMENTOS_QUEUE)
	public void receiveMessageSQS(String message) throws Exception {
		
		log.info("Iniciando processamento de mensagens pendentes...");
		LancamentoContabil lancamentoContabil = new LancamentoContabilRequestDTO().toEntity(message);
		log.info("Processando lancamento: {}", lancamentoContabil.getCodigo());
		ContaContabil contaContabilBuscada = contaContabilService.buscar(lancamentoContabil.getContaContabil());
		boolean isValid = validarLancamentoExistente(lancamentoContabil, contaContabilBuscada);
		if(!isValid) {
			List<LancamentoContabil> lancamentos = lancamentoRepository.findByContaContabilAndDataLancamentoAndValorLancamento(lancamentoContabil.getContaContabil(), lancamentoContabil.getDataLancamento(), lancamentoContabil.getValorLancamento());
			if(lancamentos.size() > 0) {
				LancamentoContabil lancamentoBuscado = lancamentos.stream().sorted(Comparator.comparing(LancamentoContabil::getId)).findFirst().get();
				inserirLancamentoDuplicado(lancamentoBuscado, lancamentoContabil);
			}
			
		} else {
			lancamentoContabil.setStatusLancamento(StatusLancamento.Lancado);
			inserirLancamentoContabil(lancamentoContabil);
		}
		log.info("Processando do lancamento: {} concluído", lancamentoContabil.getCodigo());
		
	}
	
	@Transactional
	private void inserirLancamentoDuplicado(LancamentoContabil lancamentoBuscado, LancamentoContabil lancamentoRecebido) throws Exception {
		LancamentoContabilDuplicado lancamentoDuplicado = new LancamentoContabilDuplicado();
		try {
			BeanUtils.copyProperties(lancamentoBuscado, lancamentoDuplicado, "codigo");
			lancamentoDuplicado.setCodigo(lancamentoRecebido.getCodigo());
			lancamentoDuplicado.setCodigoLancamentoOriginal(lancamentoBuscado.getCodigo());
			lancamentoDuplicadoRepository.save(lancamentoDuplicado);
		} catch (Exception ex) {
			log.error("Erro ao inserir lançamento: {} duplicado: {}", lancamentoDuplicado.getCodigo(), ex.getMessage());
		}
		
	}
	
	@Transactional
	private void inserirLancamentoContabil(LancamentoContabil lancamentoContabil) {
		lancamentoRepository.save(lancamentoContabil);
	}

	/**
	 * Realiza a validação se um determinado lançamento jã existe na base
	 * @param lancamentoContabil
	 * @param contaContabilBuscada
	 * @return {@link Boolean}
	 */
	private boolean validarLancamentoExistente(LancamentoContabil lancamentoContabil, ContaContabil contaContabilBuscada) {

		try {
			movimentacaoService.validaLancamentoExistente(contaContabilBuscada, lancamentoContabil.getDataLancamento(), lancamentoContabil.getValorLancamento());
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
		
	}
	
	/**
	 * Método responsável por salvar os lançamentos em contingência, no caso de ocorrer 
	 * algum problema no envio da mensagem a fila do SQS
	 * @param message
	 * @throws Exception
	 */
	private void salvarLancamentoContingencia(String message) throws Exception {
		LancamentoContabil lancamento = recuperarLancamento(message);
		lancamento.setStatusLancamento(StatusLancamento.Contingencia);
		if(Objects.nonNull(lancamento)) {
			movimentacaoService.validaLancamentoExistente(contaService.buscar(lancamento.getContaContabil()), lancamento.getDataLancamento(), lancamento.getValorLancamento());
			lancamentoRepository.save(lancamento);
		}
	}
	
	/**
	 * Endpoint que transforma o JSON de LancamentoContabilRequestDTO na entidade LancamentoContabil
	 * @param message
	 * @return
	 */
	private LancamentoContabil recuperarLancamento(String message) {
		return new LancamentoContabilRequestDTO().toEntity(message);
	}

}
