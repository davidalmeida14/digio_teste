package br.com.digio.domain.services.contacontabil;

import br.com.digio.api.model.ContaContabil;
/**
 * 
 * @author davidalmeida
 *
 */
public interface ContaContabilService {

	/**
	 * Método responsável por buscar uma conta contábil na base
	 * @param numeroContaContabil
	 * @return {@link ContaContabil}
	 * @throws Exception
	 */
	public ContaContabil buscar(Long numeroContaContabil) throws Exception;
}
