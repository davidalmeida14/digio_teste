package br.com.digio.domain.services.movimentacaocontabil;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.digio.api.model.ContaContabil;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.request.LancamentoContabilSearch;

/**
 * 
 * @author davidalmeida
 *
 */
@Service
public interface MovimentacaoContabilService {
	
	/**
	 * Mẽtodo responsãvel por realizar o laçamento de uma movimentação contábil
	 * @param movimentacao
	 * @return {@link LancamentoContabil}
	 * @throws Exception
	 */
	LancamentoContabil lancar(LancamentoContabilRequestDTO movimentacao) throws Exception;
	
	/**
	 * Mẽtodo responsãvel por validar se o lançamento informado jã existe na base com base na data, valor e contacontabil
	 * @param contaContabilBuscada
	 * @param data
	 * @param valor
	 * @throws Exception
	 */
	public void validaLancamentoExistente(ContaContabil contaContabilBuscada, LocalDate data, BigDecimal valor) throws Exception;

	/**
	 * Mẽtodo responsãvel por recuperar da base as informações do lançamento contábil com base no seu UUID (codigo)
	 * @param codigo
	 * @return
	 * @throws Exception
	 */
	public LancamentoContabil buscar(String codigo) throws Exception;

	/**
	 * Método responsável poela listagem de lançamentos contábeis no total e pode ser filtrado com os campos do objeto Search
	 * @param contaContabil
	 * @return {@link LancamentoContabil}
	 */
	List<LancamentoContabil> listar(LancamentoContabilSearch contaContabil);


}
