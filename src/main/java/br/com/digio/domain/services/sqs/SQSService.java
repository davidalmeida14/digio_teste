package br.com.digio.domain.services.sqs;
/**
 * 
 * @author davidalmeida
 *
 */
public interface SQSService {
	
	/**
	 * Mẽtodo responsável por enviar uma mensagem a fila do AWS SQS
	 * @param queue
	 * @param message
	 * @param tentativasEnvio
	 */
	public void sendMessage(String queue, String message, Integer tentativasEnvio);
	
	/**
	 * Mẽtodo responsãvel por escutar mensagens recebidas na fila e processá-las
	 * @param message
	 * @throws Exception
	 */
	public void receiveMessageSQS(String message) throws Exception;

}
