package br.com.digio.domain.services.estatistica.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.domain.services.estatistica.EstatisticaService;
import br.com.digio.infraestructure.repository.LancamentoContabilRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author davidalmeida
 *
 */

@Service
@Slf4j
public class EstatisticaServiceImpl implements EstatisticaService {

	@Autowired
	private LancamentoContabilRepository lancamentoRepository;

	@Override
	public ResultadoEstatistica obterEstatisticas(LancamentoContabil lancamento) throws Exception {
		ResultadoEstatistica estatistica = new ResultadoEstatistica();
		try {
			

			Example<LancamentoContabil> exmple = Example.of(lancamento);
			List<LancamentoContabil> lancamentos = lancamentoRepository.findAll(exmple);

			estatistica.setQuantidade(lancamentos.stream().count());
			estatistica.setSoma(BigDecimal
					.valueOf(lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).sum())
					.setScale(2, RoundingMode.HALF_UP));
			estatistica.setMedia(BigDecimal.valueOf(
					lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).average().orElse(0.0))
					.setScale(2, RoundingMode.HALF_UP));
			estatistica.setMinimo(BigDecimal.valueOf(
					lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).min().orElse(0.0))
					.setScale(2, RoundingMode.HALF_UP));
			estatistica.setMaximo(BigDecimal.valueOf(
					lancamentos.stream().mapToDouble(l -> l.getValorLancamento().doubleValue()).max().orElse(0.0))
					.setScale(2, RoundingMode.HALF_UP));

			
		} catch (Exception e) {
			log.error("Erro ao obter estatísticas. Erro: {} . Dados lancamento: {}",e.getMessage(), lancamento);
			throw e;
		}
		
		return estatistica;

	}
}
