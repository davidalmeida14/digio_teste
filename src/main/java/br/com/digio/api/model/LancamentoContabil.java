package br.com.digio.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.digio.api.core.DeserializerCustomizado;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Table(name = "LANCAMENTOCONTABIL")
@Entity(name = "LANCAMENTOCONTABIL")
@EqualsAndHashCode
public class LancamentoContabil {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	@EqualsAndHashCode.Include
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CODIGO")
	@ApiModelProperty(hidden = true)
	private String codigo;
	
	@Column(name = "VALORLANCAMENTO")
	@ApiModelProperty(hidden = true)
	private BigDecimal valorLancamento;
	
	@Column(name = "DATALANCAMENTO")
	@DateTimeFormat(iso = ISO.DATE, pattern = "yyyyMMdd")
	@JsonDeserialize(using = DeserializerCustomizado.class)
	@ApiModelProperty(hidden = true)
	private LocalDate dataLancamento;
	
	@Column(name = "CONTACONTABIL")
	@ApiModelProperty(hidden = false)
	private Long contaContabil;
	
	@Column(name = "STATUSLANCAMENTO")
	@ApiModelProperty(hidden = true)
	@Enumerated(EnumType.STRING)
	private StatusLancamento statusLancamento;
	
	@CreationTimestamp
	@Column(name = "DATAINSERCAO")
	@ApiModelProperty(hidden = true)
	private LocalDateTime dataInsercao;
	
	
}
