package br.com.digio.api.model;

import lombok.Getter;

@Getter
public enum StatusLancamento {

	Lancado("L"),
	Duplicado("D"),
	Contingencia("C"),
	Erro("E");
	
	public String key;
	
	private StatusLancamento(String key) {
		this.key = key;
	}
}
