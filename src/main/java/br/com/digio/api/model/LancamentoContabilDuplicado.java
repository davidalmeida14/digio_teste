package br.com.digio.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.digio.api.core.DeserializerCustomizado;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(name = "LANCAMENTOCONTABILDUPLICADO")
@EqualsAndHashCode
public class LancamentoContabilDuplicado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	@EqualsAndHashCode.Include
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "VALORLANCAMENTO")
	private BigDecimal valorLancamento;
	
	@Column(name = "DATALANCAMENTO")
	@DateTimeFormat(iso = ISO.DATE, pattern = "yyyyMMdd")
	@JsonDeserialize(using = DeserializerCustomizado.class)
	private LocalDate dataLancamento;
	
	@Column(name = "CONTACONTABIL")
	private Long contaContabil;
	
	@Column(name = "STATUSLANCAMENTO")
	@Enumerated(EnumType.STRING)
	private StatusLancamento statusLancamento;
	
	@CreationTimestamp
	@Column(name = "DATAINSERCAO")
	private LocalDateTime dataInsercao;
	
	@Column(name = "CODIGOLANCAMENTOORIGINAL")
	private String codigoLancamentoOriginal;
	
}
