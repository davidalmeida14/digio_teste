package br.com.digio.api.model;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "CONTACONTABIL")
public class ContaContabil {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "CONTACONTABIL")
	private Long contaContabil;
	
	@CreationTimestamp
	@Column(name = "DATACADASTRO")
	private OffsetDateTime dataCadastro;
	
	
}
