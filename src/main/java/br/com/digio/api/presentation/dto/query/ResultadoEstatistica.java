package br.com.digio.api.presentation.dto.query;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultadoEstatistica {
	
	private BigDecimal soma = BigDecimal.ZERO.setScale(2);
	private BigDecimal maximo = BigDecimal.ZERO.setScale(2);
	private BigDecimal minimo = BigDecimal.ZERO.setScale(2);
	private BigDecimal media = BigDecimal.ZERO.setScale(2);
	private Long quantidade;
	

}
