package br.com.digio.api.presentation.dto.response;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.digio.api.core.SerializerCustomizado;
import br.com.digio.api.model.StatusLancamento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({ "codigo"})
public class LancamentoContaContabilResponse {

	private String codigo;
	private Long contaContabil;
	@JsonFormat(pattern = "yyyyMMdd", shape = Shape.STRING)
	@DateTimeFormat(iso = ISO.DATE)
	@JsonSerialize(using = SerializerCustomizado.class)
	private LocalDate dataLancamento;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = Shape.STRING)
	@DateTimeFormat(iso = ISO.DATE_TIME, pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime dataInsercao;
	private BigDecimal valor;
	private StatusLancamento statusLancamento;
}
