package br.com.digio.api.presentation.dto.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstatisticaLancamentoContabilResponse {
	
	private BigDecimal soma;
	private BigDecimal maximo;
	private BigDecimal minimo;
	private BigDecimal media;
	private Long quantidade;

}
