package br.com.digio.api.presentation.dto.request;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.digio.api.core.SerializerCustomizado;
import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.model.StatusLancamento;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class LancamentoContabilRequestDTO {
	
	@NotNull
	@ApiModelProperty(value = "Numero da conta contábil", example = "12345")
	public Long contaContabil;
	
	@NotNull
	@JsonFormat(pattern = "yyyyMMdd", shape = Shape.STRING)
	@DateTimeFormat(iso = ISO.DATE)
	@JsonSerialize(using = SerializerCustomizado.class)
	@ApiModelProperty(value = "Data do lançamento da movimentação", example = "yyyyMMdd")
	public LocalDate dataLancamento;
	
	@NotNull
	@ApiModelProperty(value = "Valor do lançamento", example = "15.50")
	public BigDecimal valorLancamento;
	
	@JsonIgnoreProperties(allowSetters = true)
    @JsonInclude(value = Include.NON_NULL)
	@ApiModelProperty(hidden = true)
	private String codigo;
	
	/**
	 * Método responsável por converter este objeto em json String
	 * @return
	 */
	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Parse do objeto LancamentoContabilRequestDTO para a entidade LancamentoContabil
	 * @return
	 */
	public LancamentoContabil toEntity() {
		ModelMapper mapper = new ModelMapper();
		
		try {
			LancamentoContabil lancamento = mapper.map(this, LancamentoContabil.class);
			lancamento.setStatusLancamento(StatusLancamento.Duplicado);
			return lancamento;
		} catch (Exception e) {
			log.error("Ocorreu um erro ao obter lancamento: {} ", e.getMessage());
			return null;
		}
	}
	/**
	 * Parse do JSON do LancamentoRequestDTO para entidade
	 * @param json
	 * @return
	 */
	public LancamentoContabil toEntity(String json) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyyMMdd"));
		mapper.registerModule(new JavaTimeModule());
		try {
			LancamentoContabil lancamento = mapper.readValue(json, LancamentoContabil.class);
			lancamento.setStatusLancamento(StatusLancamento.Contingencia);
			return lancamento;
		} catch (Exception e) {
			log.error("Ocorreu um erro ao obter lancamento: {} - {}", e.getMessage(), json);
			return null;
		}
		
	}

}
