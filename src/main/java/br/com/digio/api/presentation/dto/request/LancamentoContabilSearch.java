package br.com.digio.api.presentation.dto.request;

import br.com.digio.api.model.StatusLancamento;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LancamentoContabilSearch {
	
	@ApiModelProperty(value = "Numero da conta contabil")
	private Long contaContabil;
	
	@ApiModelProperty(value = "Status do Lançamento")
	private StatusLancamento statusLancamento;
	
}
