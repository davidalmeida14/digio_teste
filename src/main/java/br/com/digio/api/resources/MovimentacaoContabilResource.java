package br.com.digio.api.resources;

import static br.com.digio.utils.Util.getUri;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.api.presentation.dto.request.LancamentoContabilSearch;
import br.com.digio.api.presentation.dto.response.LancamentoContaContabilCadastroResponse;
import br.com.digio.api.presentation.dto.response.LancamentoContaContabilResponse;
import br.com.digio.domain.services.movimentacaocontabil.MovimentacaoContabilService;
import br.com.digio.utils.ConstantesResourceAPI;
import br.com.digio.utils.GenericConverter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "Operações com lançamentos contábeis", tags = "Lançamentos Contábeis")
@RestController
@RequestMapping(value = ConstantesResourceAPI.ENDPOINT_LANCAMENTOS_CONTABEIS)
public class MovimentacaoContabilResource {
	
	
	@Autowired 
	public GenericConverter converter;
	
	@Autowired
	private MovimentacaoContabilService movimentacaoService;
	
	@ApiOperation(value = "Cadastro de lançamento contábil.", 
			  tags = "", 
			  notes = "Este endpoint é responsável por realizar lançamentos contábeis em sistema.",
			  response = LancamentoContaContabilCadastroResponse.class)
	@PostMapping
	public ResponseEntity<LancamentoContaContabilCadastroResponse> lancar(@RequestBody @Valid LancamentoContabilRequestDTO movimentacao) throws Exception {
		LancamentoContabil movimentoLancado = movimentacaoService.lancar(movimentacao);
		LancamentoContaContabilCadastroResponse response = montarResponse(movimentoLancado);
		return ResponseEntity.created(getUri(response.getId())).body(response);
	}
	
	@ApiOperation(value = "Busca de lançanemnto contabil.", 
			  tags = "", 
			  notes = "Este endpoint fornece informações referentes as movimentações lançadas",
			  response = LancamentoContaContabilResponse.class)
	@GetMapping("/{id}")
	public ResponseEntity<?> buscar(@PathVariable("id") String uuid) throws Exception{
		LancamentoContabil buscar = movimentacaoService.buscar(uuid);
		LancamentoContaContabilResponse response = converter.convertModelMapper(buscar, LancamentoContaContabilResponse.class);
		return ResponseEntity.ok(response);
	}
	
	@ApiOperation(value = "Listagem de lançamentos contábeis", 
				  tags = "", 
				  notes = "Este endpoint fornece informações referentes as movimentações de lançamento contábeis",
				  response = LancamentoContaContabilResponse.class)
	@GetMapping
	public ResponseEntity<?> listar(@ModelAttribute LancamentoContabilSearch contaContabil){
		
		List<LancamentoContabil> lancamentos = movimentacaoService.listar(contaContabil);
		List<LancamentoContaContabilResponse> response = lancamentos.stream()
														  .map(lancamento -> new LancamentoContaContabilResponse(lancamento.getCodigo(), lancamento.getContaContabil(), lancamento.getDataLancamento(), lancamento.getDataInsercao(), lancamento.getValorLancamento(), lancamento.getStatusLancamento()))
														  .collect(Collectors.toList());
		
		return ResponseEntity.ok(response);
	}
	
	private LancamentoContaContabilCadastroResponse montarResponse(LancamentoContabil movimentoLancado) {
		LancamentoContaContabilCadastroResponse response = new LancamentoContaContabilCadastroResponse();
		response.setId(movimentoLancado.getCodigo());
		return response;
	}
	
}
