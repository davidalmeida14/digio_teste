package br.com.digio.api.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.digio.api.model.LancamentoContabil;
import br.com.digio.api.presentation.dto.query.ResultadoEstatistica;
import br.com.digio.domain.services.estatistica.EstatisticaService;
import br.com.digio.utils.ConstantesResourceAPI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(description = "Endpoint para estatísticas de lançamentos contábies", tags = "Estatísticas")
@RestController
@RequestMapping(value = ConstantesResourceAPI.ENDPOINT_ESTATISTICAS)
public class MovimentacaoContabilEstatisticaResource {
	
	@Autowired
	private EstatisticaService estatisticaSerice;
	
	@ApiOperation(value = "Estatísticas de movimentações", tags = "",
				  notes = "Este endpoint fornece informações analíticas referente aos lançamentos realizados",
				  response = ResultadoEstatistica.class)
	@GetMapping
	public ResponseEntity<ResultadoEstatistica> obterEstatistica(@ModelAttribute LancamentoContabil lancamentoContabil) throws Exception {
		ResultadoEstatistica resultado = estatisticaSerice.obterEstatisticas(lancamentoContabil);
		return ResponseEntity.ok(resultado);
	}

}
