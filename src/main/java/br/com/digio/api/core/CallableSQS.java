package br.com.digio.api.core;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Value;

import br.com.digio.api.presentation.dto.request.LancamentoContabilRequestDTO;
import br.com.digio.domain.services.sqs.SQSService;
import br.com.digio.utils.Constantes;

public class CallableSQS implements Callable<String> {

	
	private SQSService sqsLancamentoService;

	@Value("${amazon.tentativasEnvioSQS}")
	private int tentativasEnvioSQS;

	LancamentoContabilRequestDTO lancamentoContabil = null;

	String lancamentoJson = "";

	public CallableSQS(LancamentoContabilRequestDTO lancamentoContabil, SQSService sqsLancamentoService) {
		this.lancamentoContabil = lancamentoContabil;
		this.sqsLancamentoService = sqsLancamentoService;
	}

	@Override
	public String call() throws Exception {
		String result = "Ok";
		try {
			sqsLancamentoService.sendMessage(Constantes.LANCAMENTOS_QUEUE, lancamentoContabil.toJson(), tentativasEnvioSQS);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
		
	}

}
