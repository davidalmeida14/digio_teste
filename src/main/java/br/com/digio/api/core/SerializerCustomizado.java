package br.com.digio.api.core;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class SerializerCustomizado extends StdSerializer<LocalDate>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2674344796619796782L;

	protected SerializerCustomizado() {
		this(null);
	}

	protected SerializerCustomizado(Class<LocalDate> t) {
		super(t);
	}

	@Override
	public void serialize(LocalDate data, JsonGenerator generator, SerializerProvider provider) throws IOException {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");
		String dataFormaada = data.format(format);
		generator.writeString(dataFormaada);
	}
}
