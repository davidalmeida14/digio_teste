package br.com.digio.api.core;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DeserializerCustomizado extends StdDeserializer<LocalDate> {

	private static final long serialVersionUID = -2674344796619796782L;

	protected DeserializerCustomizado() {
		this(null);
	}

	protected DeserializerCustomizado(Class<LocalDate> t) {
		super(t);
	}

	/**
	 * Método resposável por deserializar a data de lancamento para o formato solicitado (yyyyMMdd)
	 */
	@Override
	public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonNode node = p.getCodec().readTree(p);
		String data = node.asText();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate dataDate = LocalDate.parse(data, formatter);
		return dataDate;
	}

}
