package br.com.digio.api.exceptionhandler;

import static br.com.digio.api.exceptionhandler.ProblemType.DADOS_INVALIDOS;
import static br.com.digio.api.exceptionhandler.ProblemType.MENSAGEM_INCOMPREENSIVEL;
import static br.com.digio.api.exceptionhandler.ProblemType.RECURSO_NAO_ENCONTRADO;
import static br.com.digio.api.exceptionhandler.ProblemType.REGISTRO_EXISTENTE;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import com.sun.net.httpserver.Headers;

import br.com.digio.domain.exception.EntidadeInexistenteException;
import br.com.digio.domain.exception.LancamentoExistenteException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	
	@Autowired
	private MessageSource messageSource;
	
	private static final String MSG_ERRO_INESPERADO = "Ocorreu um erro inesperado. Tente novamente e se o erro persistir, entre em contato com o administrador do sistema.";
	
	/**
	 * Trata exceções de entidades inexistents (Registros não encontrados) 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(EntidadeInexistenteException.class)
	public ResponseEntity<?> handleEntidadeInexistenteException(EntidadeInexistenteException ex, WebRequest request){
		Integer status = HttpStatus.NOT_FOUND.value();
		Problem problem = createProblemBuilder(status, RECURSO_NAO_ENCONTRADO, ex.getMessage()).build();
		return handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	/**
	 * Trata lançamentos contábeis que já foram inseridos na base 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(LancamentoExistenteException.class)
	public ResponseEntity<?> handleLancamentoExistenteException(LancamentoExistenteException ex, WebRequest request){
		Integer status = HttpStatus.BAD_REQUEST.value();
		Problem problem = createProblemBuilder(status, REGISTRO_EXISTENTE, ex.getMessage()).build();
		return handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	/**
	 * Trata requisições a URLs que não estão mapeadas/tratadas
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		Problem problem = createProblemBuilder(status.value(), ProblemType.RECURSO_NAO_ENCONTRADO, "O recurso buscado não foi encontrado").build();
		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	/**
	 * Tratativa quando se consome um endpoint com um verbo que não é suportado
	 * @param ex
	 * @param req
	 * @param methodNotAllowed
	 * @param headers
	 * @return
	 */
	private ResponseEntity<Object> handleHttpRequestMethodNotSupportedException(Exception ex, WebRequest req,
			HttpStatus methodNotAllowed, Headers headers) {
		String detail = String.format("Verbo HTTP %s não é aceito para este endpoint", "POST");
		HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;
		Problem problem = createProblemBuilder(status.value(), ProblemType.VERBO_NAO_PERMITIDO, detail).build();
		return super.handleExceptionInternal((HttpRequestMethodNotSupportedException) ex, problem, new HttpHeaders(), status, req);
	}

	/**
	 * Handler responsável por tratar exceções de argumentos da request inválidos
	 * 
	 * {@link MethodArgumentNotValidException}
	 * {@link HttpHeaders}
	 * {@link HttpStatus}
	 * {@link WebRequest}
	 * 
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		BindingResult bindingResult = ex.getBindingResult();

		/**
		 * Construção dos detalhes de cada validação que falhou. Cada validação falha é
		 * mapeada para um Problem.Field, utilizando o messageSource para pegar a
		 * descrição do erro contido no message.properties
		 *
		 */
		List<Problem.Object> problemObjects = bindingResult.getAllErrors().stream().map(objectError -> {

			String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());

			String name = objectError.getObjectName();

			if (objectError instanceof FieldError) {
				name = ((FieldError) objectError).getField();
			}
			return Problem.Object.builder().name(name).userMessage(message).build();

		}).collect(Collectors.toList());

		String detail = String
				.format("Um ou mais dados estão inválidos. Faça o preenchimento correto e tente novamente");

		Problem problem = createProblemBuilder(status.value(), DADOS_INVALIDOS, detail).userMessage(detail)
				.objects(problemObjects).build();

		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	/**
	 * Trata inconsistências de dados recebidos na API
	 * {@link HttpMessageNotReadableException}
	 * {@link HttpHeaders}
	 * {@link HttpStatus}
	 * {@link WebRequest}
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String detail = "O corpo da requisição está inválido. Verifique erros de sintaxe";

		Throwable rootCause = ExceptionUtils.getRootCause(ex);

		if (rootCause instanceof InvalidFormatException) {
			return handleInvalidFormatException((InvalidFormatException) rootCause, headers, status, request);
		} else if (rootCause instanceof PropertyBindingException) {
			return handleUnrecognizedPropertyException((PropertyBindingException) rootCause, headers, status, request);
		} else if (rootCause instanceof DateTimeParseException) {
			return handleDateTimeParseException((DateTimeParseException) rootCause, headers, status, request);
		} else if (rootCause instanceof JsonParseException) {
			return handleJsonParseException((JsonParseException) rootCause, headers, status, request);
		}
		Problem problem = createProblemBuilder(status.value(), MENSAGEM_INCOMPREENSIVEL, detail).userMessage(detail)
				.build();

		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	/**
	 * Trata problemas em parse de JSON
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	private ResponseEntity<Object> handleJsonParseException(JsonParseException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		try {
			String nomePropriedade = ex.getProcessor().currentName();
			String mensagem = String.format("A propriedade %s está incorreta. Verifique o valor informado e tente novamente.", nomePropriedade);
			Problem build = createProblemBuilder(status.value(), ProblemType.DADOS_INVALIDOS, mensagem).build();
			return handleExceptionInternal(ex, build, headers, status, request);
		} catch (IOException e) {
			return handleExceptionInternal(ex, null, headers, status, request);
		}
	}
	
	/**
	 * Faz a tratativa de inconsistências de propriedaes passadas na requisição
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		String path = joinPath(ex.getPath());

		String detail = String
				.format("A propriedade '%s' recebeu o valor '%s' que é um tipo inválido. Corrija e informe "
						+ "um valor do tipo %s", path, ex.getValue(), ex.getTargetType().getSimpleName());

		Problem problem = createProblemBuilder(status.value(), MENSAGEM_INCOMPREENSIVEL, detail)
				.userMessage(MSG_ERRO_INESPERADO).build();

		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	/**
	 * Tratativa para problema de parse de data 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	private ResponseEntity<Object> handleDateTimeParseException(DateTimeParseException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String detail = "Não foi possível fazer o parse da data: " + ex.getParsedString() + ". Corrija e tente novamente.";
		
		Problem problem = createProblemBuilder(status.value(), DADOS_INVALIDOS, detail)	
										.userMessage(detail)
										.build();

		return handleExceptionInternal(ex, problem, headers, status, request);
		
	}

	/**
	 * Tratativa para propriedades que não estão mapeadas na requisição
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	private ResponseEntity<Object> handleUnrecognizedPropertyException(PropertyBindingException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		String path = joinPath(ex.getPath());

		String detail = String
				.format("A propriedade: '%s' não existe. Corrija ou remova essa propriedade e tente novamente", path);

		Problem problem = createProblemBuilder(status.value(), ProblemType.DADOS_INVALIDOS, detail)
				.userMessage(MSG_ERRO_INESPERADO).build();

		return handleExceptionInternal(ex, problem, headers, status, request);
	}

	/**
	 * Trataiva genérica de erros
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		if(ex instanceof HttpRequestMethodNotSupportedException) {
			return handleHttpRequestMethodNotSupportedException(ex, request, HttpStatus.METHOD_NOT_ALLOWED, new Headers());
		}
		if (body == null) {
			body = Problem.builder().timestamp(LocalDateTime.now()).title(status.getReasonPhrase())
					.status(status.value()).userMessage(MSG_ERRO_INESPERADO).build();
			
		} else if (body instanceof String) {
			body = Problem.builder().timestamp(LocalDateTime.now()).title((String) body).status(status.value())
					.userMessage(MSG_ERRO_INESPERADO).build();
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	/**
	 * Método responsável por retornar um build do objeto Problem que será utilizado no corpo da resposta do erro;
	 * @param status
	 * @param problemType
	 * @param detail
	 * @return
	 */
	private Problem.ProblemBuilder createProblemBuilder(Integer status, ProblemType problemType, String detail) {
		return Problem.builder()
					  .status(status)
					  .type(problemType.getUri())
					  .timestamp(LocalDateTime.now())
					  .title(problemType.getTitle())
					  .detail(detail);
	}
	
	private String joinPath(List<Reference> path) {
		return path.stream().map(ref -> ref.getFieldName()).collect(Collectors.joining("."));
	}
}
