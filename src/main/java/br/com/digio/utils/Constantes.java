package br.com.digio.utils;

public class Constantes {
	public static final String STATUS_DUPLICADO = "D";
	public static final String STATUS_CONTINGENCIA = "C";
	public static final String STATUS_LANCADO = "L";
	public static final String LANCAMENTOS_QUEUE = "lancamentosQueue";

}
