package br.com.digio.utils;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericConverter {

	
	@Autowired
	private ModelMapper modelMapper;
	public <E, T> E convertModelMapper(T source, Class<E> typeDestination) {
		return convertModelMapper(source, typeDestination, MatchingStrategies.STANDARD);
	}

	private <E, T> E convertModelMapper(T source, Class<E> typeDestination, MatchingStrategy strategy) {
		E model = null;
		if (source != null && typeDestination != null) {

			modelMapper = new ModelMapper();

			modelMapper.getConfiguration().setMatchingStrategy(strategy);
			model = modelMapper.map(source, typeDestination);
		}

		return model;
	}
	

}
