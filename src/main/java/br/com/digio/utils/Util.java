package br.com.digio.utils;

import java.net.URI;
import java.util.UUID;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class Util {
	
	public static String gerarUUID() {
		return UUID.randomUUID().toString();
	}
	
	public static void checkThrow(boolean instrution, Exception ex) throws Exception {
		if(instrution ) {
			throw ex;
		}
	}
	
	public static URI getUri(String id) {
		URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();
		return location;
	}
}
