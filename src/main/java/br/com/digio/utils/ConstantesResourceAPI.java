package br.com.digio.utils;

public class ConstantesResourceAPI {
	public final static  String ENDPOINT_API = "/api";
	public final static String VERSION_V1 = "/v1";
	public final static String ENDPOINT_LANCAMENTOS_CONTABEIS = ConstantesResourceAPI.VERSION_V1 + ConstantesResourceAPI.ENDPOINT_API + "/lancamentos-contabeis";
	public final static String ENDPOINT_ESTATISTICAS = ENDPOINT_LANCAMENTOS_CONTABEIS + "/stats";

}
