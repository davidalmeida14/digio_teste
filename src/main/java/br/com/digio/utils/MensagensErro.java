package br.com.digio.utils;

public class MensagensErro {
	
	public static final String LANCAMENTO_NAO_ENCONTRADO = "O lancamento de codigo %s não foi encontrado"; 
	public static final String LANCAMENTO_EXISTENTE = "Já existe um lançamento para a conta: %s , na data %s no valor: %s";

}
