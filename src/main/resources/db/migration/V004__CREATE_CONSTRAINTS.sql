
-- ----------------------------
-- Auto increment value for CONTACONTABIL
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[CONTACONTABIL]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table CONTACONTABIL
-- ----------------------------
ALTER TABLE [dbo].[CONTACONTABIL] ADD CONSTRAINT [PK__CONTACON__3213E83F0FDA7E99] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for LANCAMENTOCONTABIL
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[LANCAMENTOCONTABIL]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table LANCAMENTOCONTABIL
-- ----------------------------
ALTER TABLE [dbo].[LANCAMENTOCONTABIL] ADD CONSTRAINT [PK__LANCAMEN__3214EC27D1AE5FFF] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for LANCAMENTOCONTABILDUPLICADO
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[LANCAMENTOCONTABILDUPLICADO]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table LANCAMENTOCONTABILDUPLICADO
-- ----------------------------
ALTER TABLE [dbo].[LANCAMENTOCONTABILDUPLICADO] ADD CONSTRAINT [PK__LANCAMEN__3214EC27B8746C3A] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO
