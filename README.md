# DIGIOCONTABIL
- O DigioContabil é uma API Rest criada com propósito de teste para o processo seletivo no Digio. A ideia é poder realizar lançamentos contábeis em contas contábeis com valores e datas diferentes. Além disso, é possível recuperar essas informações e obter estatísticas.

# Lançamentos Contabeis

Através deste recurso, será possível gerenciar os lançamentos contábeis.

* GET /v1/api/lancamentos-contabeis - Listagem de todos os lançamentos contábeis (Pode-se utilizar os filtros de contaContabil e statusLancamento
* POST /v1/api/lancamentos-contabeis - Realiza um inserção de movimentação contábil para uma determinada conta.
* GET /v1/api/lancamentos-contabeis/{id} - Recupera as informações do lançamento contábil cadastrado anteriormente.
* GET /v1/api/lancamentos-contabeis/stats - Obtém estatísticas dos lançamentos contábeis de toda base ou de uma conta contábil específica, podendo ser filtrada via queryParam.

# Observações
-
-A Inserção de uma nova movimentação é feita de forma assíncrona via mensageria (AWS SQS),a fim de reduzir possíveis duplicidades, sendo feito um double check se já existe o lançamento antes do envio para a fila e assim que o serviço recupera a mensagem da fila para processamento.
-Mesmo assim, uma tabela foi inserida para que, caso esse cenário ocorra, ficará registrado o lançamento duplicado e qual o original, facilitando a rastreabilidade.

# Pontos de Melhoria
* Paginação do resultado na busca de todos lançamentos
* Separação da API e do recurso que consome a fila do SQS em dois microserviços, a fim de aumentar a escalabilidade e resiliência.
* Utilização de Mocks para simular objetos complexos e objetos de integrações.
